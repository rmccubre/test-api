# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 15:22:46 2022

@author: rmccubre

    Python REST API Tutorial - Building a Flask REST API

"""

from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///database.db'

db = SQLAlchemy(app)

class VideoModel(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100),nullable=False)
    views = db.Column(db.Integer, nullable =False)
    likes = db.Column(db.Integer, nullable =False)
    
    def __repr__(self):
        return str(self.name)+" "+str(self.views)+" "+str(self.likes)

#Remove this from environment db.create_all()

#automatically parse through request that was sent and parse arguments.
video_put_args = reqparse.RequestParser()
video_put_args.add_argument("name",type=str, help="Please use a string",required=True)
video_put_args.add_argument("views",type=int, help="Please use a int!",required=True)
video_put_args.add_argument("likes",type=int, help="Please use a int!?",required=True)




#stores names! holy cow!
names = {"tim":  { "age": 19, "gender": "male"},
         "bill": { "age": 70, "gender": "male"}}

class HelloWorld(Resource):
    def get(self,name):
        return names[name]
    
    def post(self):
        return {"data": "Posted"}


videos={}

def abort_if_video_id_doesnt_exist(video_id):
    if video_id not in videos:
        abort(404,message="Video id is not valid!")

def abort_if_video_exist(video_id):
    if video_id in videos:
        abort(409,message="Video already exsist with that ID..")


class Video(Resource):
    def get(self,video_id):
        abort_if_video_id_doesnt_exist(video_id)
        return videos[video_id]
    
    def put(self,video_id):
        abort_if_video_exist(video_id)
        args = video_put_args.parse_args()
        videos[video_id]= args
        return videos[video_id], 201 #created.
    
    def delete(self,video_id):
        abort_if_video_id_doesnt_exist(video_id)
        del videos[video_id]
        return 'deleted', 204
                   
                   
api.add_resource(Video, "/Video/<int:video_id>")



if __name__ == "__main__":
        app.run(debug=True)   #Never run in a production env.
        