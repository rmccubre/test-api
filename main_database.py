# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 15:22:46 2022

@author: rmccubre

    Python REST API Tutorial - Building a Flask REST API

"""

from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///database.db'

db = SQLAlchemy(app)

class VideoModel(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100),nullable=False)
    views = db.Column(db.Integer, nullable =False)
    likes = db.Column(db.Integer, nullable =False)
    
    def __repr__(self):
        return str(self.name)+" "+str(self.views)+" "+str(self.likes)

#db.create_all()

#automatically parse through request that was sent and parse arguments.
video_put_args = reqparse.RequestParser()
video_put_args.add_argument("name",type=str, help="Please use a string",required=True)
video_put_args.add_argument("views",type=int, help="Please use a int!",required=True)
video_put_args.add_argument("likes",type=int, help="Please use a int!?",required=True)

video_update_args = reqparse.RequestParser()
video_update_args.add_argument("name",type=str, help="Please use a string")
video_update_args.add_argument("views",type=int, help="Please use a int!")
video_update_args.add_argument("likes",type=int, help="Please use a int!?")



resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'views': fields.Integer,
    'likes': fields.Integer
    }


class Video(Resource):
    @marshal_with(resource_fields)
    def get(self,video_id):
        result = VideoModel.query.get_or_404(video_id)
        return result
    
    @marshal_with(resource_fields)
    def put(self,video_id):
        args = video_put_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if result:
            abort(409,message = "Video id Taken")
        video = VideoModel(id =video_id, likes=args['likes'], 
                           name= args['name'],views=args['views'])
        db.session.add(video)
        db.session.commit()
        return video, 201 #created.
    
    @marshal_with(resource_fields)
    def patch(self, video_id):
        args = video_update_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if not result:
            abort(404,"Video doesn't exsit, cannot update")
        
        # if result.name is None:
        #     abort(404,"empty name")
            
        if args['name']:
            result.name = args['name']
        if args['views']:
            result.views = args['views']
        if args['likes']:
            result.likes = args['likes']
        
        db.session.commit()
        return result, 201
    
    # def delete(self,video_id):
    #     abort_if_video_id_doesnt_exist(video_id)
    #     del videos[video_id]
    #     return 'deleted', 204
                   
                   
api.add_resource(Video, "/Video/<int:video_id>")



if __name__ == "__main__":
        app.run(debug=True)   #Never run in a production env.
        