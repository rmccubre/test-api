# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 16:02:54 2022

@author: rmccubre
requests
"""

import requests

BASE= "http://127.0.0.1:5000/"

data=[ {"likes":78,"name":"Joe","views": 10},
       {"likes":2000,"name":"How to Make REST API","views": 50},
       {"likes":30,"name":"Tim","views": 2000},
       {"likes":35,"name":"Time","views": 70}]

for i in range(len(data)):
    response=requests.put(BASE + "Video/"+str(i),data[i])
    print(response.json())

response=requests.get(BASE + "Video/2")
print(response.json())


#response = requests.patch(BASE + "Video/2", {"views":9999, "likes":101})

response = requests.patch(BASE + "Video/7", {})
print(response.json())
# for i in range(len(data)):
#       response=requests.get(BASE + "Video/"+str(i))
#       print(response)



#input()
# response=requests.get(BASE + "Video/2")
# print(response)